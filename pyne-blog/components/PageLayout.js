import { Container, Row, Col } from "react-bootstrap";
import Navbar from "./Navbar";
import { useTheme } from "providers/ThemeProvider";

import { FaFacebookF, FaInstagram, FaLinkedinIn } from "react-icons/fa";

import Image from "next/image";

import appleIcon from "../public/apple-store.png";
import googleIcon from "../public/google-play.png";
import brandPyne from "../public/pyneBrandWhite.png";

export default function PageLayout({ children, className }) {
  const { theme, toggleTheme } = useTheme();

  return (
    <div className={theme.type}>
      <div className="page-header">
        <Navbar theme={theme} toggleTheme={toggleTheme} />
      </div>
      <Container>
        <main>
          <div className={`page-wrapper ${className}`}>{children}</div>
        </main>
      </Container>
      <footer className="page-footer mt-5">
        <Container>
          <Row>
            <Col xs={6} md={3} className="wrapper-footer-brand">
              <a href="https://pyneapp.com/" target="_blank">
                <Image
                  className="image-stores"
                  src={brandPyne}
                  alt="Image Pyne"
                  width={80}
                  height={40}
                />
              </a>
              <p className="footer-notes">Bem-vindo à Inovação!</p>
            </Col>
            <Col xs={12} md={3}>
              <p className="footer-note-title">SIGA-NOS</p>
              <div className="footer-social-icons">
                <span className="social-icons-link">
                  <a
                    href="https://www.linkedin.com/company/pyneapp-ex/"
                    target="_blank"
                  >
                    <FaLinkedinIn />
                  </a>
                </span>
                <span className="social-icons-link">
                  <a href="https://www.facebook.com/pyneapp/" target="_blank">
                    <FaFacebookF />
                  </a>
                </span>
                <span className="social-icons-link">
                  <a
                    href="https://www.instagram.com/pyneapp.br/?igshid=1ujgrzwc5zkxf"
                    target="_blank"
                  >
                    <FaInstagram />
                  </a>
                </span>
              </div>
            </Col>
            <Col xs={12} md={3}>
              <p className="footer-note-title">ENTRAR EM CONTATO!</p>
              <p className="footer-notes">contact@pyneapp.com</p>
              <p className="footer-notes">São Paulo, Brasil</p>
              <p className="footer-notes">Nova Iorque, Estados Unidos</p>
              <a href="https://pyneapp.com/privacy" target="_blank">
                <p className="footer-notes">Política de Privacidade</p>
              </a>
              <a href="https://pyneapp.com/terms" target="_blank">
                <p className="footer-notes">Termos e Condições</p>
              </a>
            </Col>
            <Col xs={12} md={3}>
              <a
                href="https://play.google.com/store/apps/details?id=com.pyne.portal"
                target="_blank"
              >
                <Image
                  className="image-stores"
                  src={googleIcon}
                  alt="Image Google Play"
                  width={153}
                  height={60}
                />
              </a>
              <a
                href="https://apps.apple.com/us/app/pyne-app/id1539983536?ign-mpt=uo%3D2"
                target="_blank"
              >
                <Image
                  className="image-stores"
                  src={appleIcon}
                  alt="Image Apple store"
                  width={153}
                  height={60}
                />
              </a>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={12}>
              <p className="footer-notes text-center mt-5">
                {new Date().getFullYear()} - Todos os direitos reservados
              </p>
            </Col>
          </Row>
        </Container>
      </footer>
      <style jsx global>
        {`
          html,
          body {
            background: ${theme.background};
            color: ${theme.fontColor};
            transition: color 0.2s ease-out 0s, background 0.2s ease-out 0s;
          }
        `}
      </style>
    </div>
  );
}
