import { Navbar, Nav, Row, Col, Container } from "react-bootstrap";
import Link from "next/link";
import ThemeToggle from "components/ThemeToggle";
import { GiHamburgerMenu } from "react-icons";
import { FaFacebookF, FaInstagram, FaLinkedinIn } from "react-icons/fa";
import { BsList } from "react-icons/bs";

import Image from "next/image";

import brandPyne from "../public/pyneBrandWhite.png";

const BlogNavbar = ({ theme, toggleTheme }) => {
  return (
    <>
      <div className="desktop-list">
        <Container>
          <Row>
            <Col className="wrapper-brand-header">
              <Link href="/">
                <Image
                  className="image-stores"
                  src={brandPyne}
                  alt="Image Pyne"
                  width={80}
                  height={40}
                />
              </Link>
            </Col>
            <Col xs={8} className="wrapper-nav-header">
              <a className="nav-header-links" href="https://pyneapp.com/">
                Home
              </a>
              <a
                className="nav-header-links"
                href="https://pyneapp.com/our-solutions"
              >
                Nossas Soluções
              </a>
              <a className="nav-header-links" href="https://pyneapp.com/why">
                Por que Pyne?
              </a>
              <Link href="/">
                <a className="nav-header-links">Blog</a>
              </Link>
              <a className="nav-header-links" href="https://pyneapp.com/plans">
                Planos
              </a>
              <a
                className="nav-header-links"
                href="https://pyneapp.com/contact"
              >
                Contato
              </a>
              <a
                className="nav-header-links link-button"
                target="_blank"
                href="https://portal.pyneapp.com/login?origin=site"
              >
                Login
              </a>
            </Col>
            <Col className="wrapper-nav-header">
              <Nav className="mt-2">
                <ThemeToggle onChange={toggleTheme} />
                <Nav.Link
                  as={() => (
                    <Link href="/">
                      <a className="fj-navbar-item fj-navbar-link"></a>
                    </Link>
                  )}
                />
              </Nav>
            </Col>
          </Row>
        </Container>
      </div>
      <div class="mobile-list">
        {/* Mobile */}
        <Navbar
          variant={theme.type}
          className="fj-navbar fj-nav-base"
          bg="transparent"
          expand="lg"
        >
          <Navbar.Brand className="fj-navbar-brand">
            <Link href="/">
              <Image
                className="image-stores"
                src={brandPyne}
                alt="Image Pyne"
                width={80}
                height={40}
              />
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav">
            <BsList className="icon-mobile" />
          </Navbar.Toggle>
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav bg="light">
              <ThemeToggle onChange={toggleTheme} />
              <Nav.Link
                as={() => (
                  <div className="navbar-list-mobile">
                    <a className="nav-header-links" href="https://pyneapp.com/">
                      Home
                    </a>
                    <a
                      className="nav-header-links"
                      href="https://pyneapp.com/our-solutions"
                    >
                      Nossas Soluções
                    </a>
                    <a
                      className="nav-header-links"
                      href="https://pyneapp.com/why"
                    >
                      Por que Pyne?
                    </a>
                    <Link href="/">
                      <a className="nav-header-links">Blog</a>
                    </Link>
                    <a
                      className="nav-header-links"
                      href="https://pyneapp.com/plans"
                    >
                      Planos
                    </a>
                    <a
                      className="nav-header-links"
                      href="https://pyneapp.com/contact"
                    >
                      Contato
                    </a>
                    <a
                      className="nav-header-links link-button"
                      target="_blank"
                      href="https://portal.pyneapp.com/login?origin=site"
                    >
                      Login
                    </a>
                  </div>
                )}
              />
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>
    </>
  );
};

export default BlogNavbar;
