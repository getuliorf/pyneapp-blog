import { Row, Col, Media, Image } from "react-bootstrap";

const AuthorIntro = () => (
  <Row>
    <Col md="8">
      {/* AUTHOR INTRO STARTS */}
      <Media className="mb-4 admin-intro">
        {/*         <Image
          roundedCircle
          width={64}
          height={64}
          className="mr-3"
          src="./pyne-admin-avatar.png"
          alt="Generic placeholder"
        /> */}
        <Media.Body className="mt-5">
          <h5 className="font-weight-bold mb-0">
            Seja Bem-vindo ao Blog da Pyne,
          </h5>
          <p className="welcome-text mt-3">
            Ajudamos negócios de todos os tipos a gerenciar licenças, cronograma
            de colaboradores, marcação de ponto, comunicação interna e muito
            mais.
          </p>
        </Media.Body>
      </Media>
      {/* AUTHOR INTRO ENDS */}
    </Col>
  </Row>
);

export default AuthorIntro;
